import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import LoginView from '../views/LoginView.vue';
import RegView from '../views/RegView.vue';
import TreeView from '../views/TreeView.vue';
import TreeUpdateView from '../views/TreeUpdateView.vue';
import TreeCreateView from '../views/TreeCreateView.vue';
import TreeDeleteView from '../views/TreeDeleteView.vue';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView
        },
        {
            path: '/login',
            name: 'login',
            component: LoginView
        },
        {
            path: '/reg',
            name: 'reg',
            component: RegView
        },
        {
            path: '/tree',
            name: 'tree',
            component: TreeView
        },
        {
            path: '/tree/update/:id',
            name: 'tree_update',
            component: TreeUpdateView
        },
        {
            path: '/tree/delete/:id',
            name: 'tree_delete',
            component: TreeDeleteView
        },
        {
            path: '/tree/create/:parent_id',
            name: 'tree_create',
            component: TreeCreateView
        }
    ]
});

export default router;
