import client from '@/api/ApiService.js';

export async function list() {
    var result = '';
    await client.get(`family-tree/get`)
        .then(response => {
            result = response.data;
        })
        .catch(error => console.log(error));
    return result;
}

export async function create(second_name, first_name, id) {
    return await client.post(`family-tree/create`,
        {
            second_name: second_name,
            first_name: first_name,
            parent_id: id
        });
}

export async function update(second_name, first_name, id) {
    return await client.post(`family-tree/update`, {
        second_name: second_name,
        first_name: first_name,
        id: id
    });
}

export async function close(id) {
    return await client.get(`family-tree/close/`+id);
}

export async function one(id) {
    var result = '';
    await client.get(`family-tree/one/` + id,)
        .then(response => {
            result = response.data;
        })
        .catch(error => console.log(error));
    return result;
}

export default {
    list,
    create,
    update,
    close,
    one
};
