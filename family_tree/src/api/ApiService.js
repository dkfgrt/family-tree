import axios from 'axios';

const client = axios.create({
    baseURL: import.meta.env.VITE_DOMAIN
});
client.interceptors.request.use(
    function (config) {
        const token = localStorage.getItem('token');
        console.log(token);
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);


export default client;
