import client from '@/api/ApiService.js';

export async function login(email, password) {
    const response = await client.post(`login`, {
        email: email,
        password: password
    });
    localStorage.setItem('token', response.data.token);
    window.location.href = '/tree';
}

export async function reg(email, password, name) {
    return await client.post(`reg`, {
        email: email,
        password: password,
        name: name
    });
}

export async function info() {
    const response = await client.get(`persons`);
    return response.data;
}

export default {
    login,
    reg,
    info
};
