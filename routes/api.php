<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PassportAuthController;
use \App\Http\Controllers\Api\FamilyTreeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('v1/reg', [PassportAuthController::class, 'signup']);
Route::post('v1/login', [PassportAuthController::class, 'login']);

Route::group(['prefix' => 'v1', 'middleware' => 'auth:sanctum'], function() {
    Route::get('get-user', [PassportAuthController::class, 'userInfo']);
    Route::group(['prefix' => 'family-tree' ], function() {
        Route::get('get', [FamilyTreeController::class, 'get']);
        Route::get('one/{id}', [FamilyTreeController::class, 'one'])
            ->where('id', '[0-9]+');
        Route::get('close/{id}', [FamilyTreeController::class, 'close']);
        Route::post('create', [FamilyTreeController::class, 'create']);
        Route::post('update', [FamilyTreeController::class, 'update']);
    });
});

