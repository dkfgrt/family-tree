<?php

namespace App\Services;

use App\Models\FamilyTree;
use App\Models\Person;

class FamilyTreeService
{

    /**
     * @param array $input
     *
     * @return FamilyTree
     * @throws \Exception
     */
    public function create(array $input): FamilyTree
    {
        \DB::beginTransaction();
        try {
            $input['user_id']          = $this->getUserId();
            $input['parent_id']        = $input['parent_id'] ?? null;
            $input['first_person_id']  = $this->createPerson($input['first_name'] ?? null);
            $input['second_person_id'] = $this->createPerson($input['second_name'] ?? null);
            $familyTree                = FamilyTree::create($input);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }
        return $familyTree;
    }

    /**
     * @param array $input
     *
     * @return FamilyTree
     * @throws \Exception
     */
    public function update(array $input): FamilyTree
    {
        \DB::beginTransaction();
        try {
            $familyTree                   = FamilyTree::where('id', $input['id'])->first();
            $familyTree->first_person_id  = $this->updatePerson($familyTree->first_person_id, $input['first_name'] ?? null);
            $familyTree->second_person_id = $this->updatePerson($familyTree->second_person_id, $input['second_name'] ?? null);
            $familyTree->save();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }
        return $familyTree;
    }

    /**
     * @param int         $person_id
     * @param string|null $name
     *
     * @return int|null
     */
    private function updatePerson(?int $person_id, ?string $name): ?int
    {
        $person = null;
        if($person_id){
            $person = Person::where('id', $person_id)->first();
        }

        if ($person instanceof Person) {
            if (empty($name)) {
                $person->delete();
                return null;
            } else {
                $person->name = $name;
                $person->save();
                return $person->id;
            }
        }
        return $this->createPerson($name);
    }

    /**
     * @param string|null $name
     *
     * @return int|null
     */
    private function createPerson(?string $name): ?int
    {

        if (empty($name)) {
            return null;
        }

        $person = Person::create([
            'name'    => $name,
            'user_id' => $this->getUserId(),
        ]);

        return $person->id ?? null;
    }

    /**
     * @return int
     */
    private function getUserId(): int
    {
        return auth()->id();
    }
}
