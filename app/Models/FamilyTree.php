<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Kalnoy\Nestedset\QueryBuilder;
use Kalnoy\Nestedset\NodeTrait;

class FamilyTree extends Model
{
    use HasFactory;
    use NodeTrait;


    protected $fillable = ['user_id', 'first_person_id', 'second_person_id', 'parent_id'];

    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeAuthUser($query): QueryBuilder
    {
        return $query->where('user_id', auth()->id());
    }

    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeIsRoot($query): QueryBuilder
    {
        return $query->where('parent_id', null);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function firstPerson(): HasOne
    {
        return $this->hasOne(Person::class, 'id', 'first_person_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function secondPerson(): HasOne
    {
        return $this->hasOne(Person::class, 'id', 'second_person_id');
    }

    public function childrenUser(): HasMany{
        return $this->hasMany(self::class,'parent_id', 'id');
    }
}
