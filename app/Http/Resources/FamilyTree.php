<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FamilyTree extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [$this->resource($this)];
    }

    private function resource($familyTree)
    {
        return [
            'first_person'  => $familyTree->firstPerson,
            'second_person' => $familyTree->secondPerson,
            'id'            => $familyTree->id,
            'children'      => $this->getChildrenNodes($familyTree),
        ];
    }


    private function getChildrenNodes($familyTree): array
    {
        $childrens = $familyTree->childrenUser;
        $result    = [];
        foreach ($childrens as $children) {
            $result[] = $this->resource($children);
        }
        return $result;
    }
}
