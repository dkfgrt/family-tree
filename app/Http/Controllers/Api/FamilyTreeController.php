<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FamilyTreeCollection;
use App\Models\FamilyTree;
use App\Rules\PersonNameValidation;
use App\Services\FamilyTreeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FamilyTreeController extends Controller
{
    public function get()
    {
        $tree = FamilyTree::authUser()->isRoot()->first();

        if($tree instanceof  FamilyTree){
            return response()->json(new \App\Http\Resources\FamilyTreeOne($tree), 200);
        }

        return response()->json(['error' =>'Пусто'],422);
    }

    public function one(Request $request)
    {
        $id = $request->route('id');
        if (empty($id)) {
            return response()->json(['error' => "Miss param id"], 422);
        }

        $tree = FamilyTree::authUser()->where('id', $id)->first();

        if($tree instanceof  FamilyTree){
            return response()->json(new \App\Http\Resources\FamilyTreeOne($tree), 200);
        }

        return response()->json([], 200);
    }


    public function create(Request $request, FamilyTreeService $ftService)
    {
        $input = $request->all();

        if(empty($input['parent_id'])){
            $validator = Validator::make($input, [
                'first_name'  => 'required|string',
                'second_name' => [new PersonNameValidation()],
            ]);
        }else{
            $validator = Validator::make($input, [
                'first_name'  => 'required|string',
                'second_name' => [new PersonNameValidation()],
                'parent_id'   => 'required|integer|exists:family_trees,id',
            ]);
        }


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        try {
            $item = $ftService->create($input);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unknown error save'], 500);
        }

        return response()->json($item, 200);
    }


    public function update(Request $request, FamilyTreeService $ftService)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'first_name'  => 'required|string',
            'second_name' => [new PersonNameValidation()],
            'id'          => 'required|integer|exists:family_trees,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        try {
            $item = $ftService->update($input);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unknown error save'], 500);
        }

        return response()->json($item, 200);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function close(Request $request)
    {
        $id = $request->route('id');

        if (empty($id)) {
            return response()->json(['error' => "Miss param id"], 422);
        }

        $item = FamilyTree::authUser()->where('id', $id)->first();

        if ($item instanceof FamilyTree) {
            if ($item->children->count()) {
                return response()->json(['error' => "Нельзя удалить если есть потомки"], 422);
            } else {
                $item->delete();
                return response()->json([], 200);
            }
        }

        return response()->json(['error' => "Такая запись не найдена"], 422);
    }
}
