<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilyTreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_trees', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('first_person_id')->comment("name");
            $table->integer('second_person_id')->nullable()->comment("name");
            \Kalnoy\Nestedset\NestedSet::columns($table);
            $table->timestamps();
        });

        Schema::create('persons', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('name', 100)->comment("name");
            $table->string('image', 100)->nullable()->comment("name");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_trees');
        Schema::dropIfExists('persons');
    }
}
